import 'package:flutter/material.dart';
import 'package:food_agenda/controllers/habit_controller.dart';
import 'package:food_agenda/db/db_helper.dart';
import 'package:food_agenda/services/theme_services.dart';
import 'package:food_agenda/ui/home_page.dart';
import 'package:food_agenda/ui/theme.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await DBHelper.initDb();
  await GetStorage.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context){
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: Themes.dark,
      darkTheme: Themes.light,
      themeMode: ThemeServices().theme,
      home: const HomePage(title: 'Food Agenda'),
    );
  }
}
