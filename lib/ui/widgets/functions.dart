// // ====================== home

// ====================== dashbord

import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:flutter/material.dart';
import 'package:food_agenda/ui/theme.dart';
import 'package:google_fonts/google_fonts.dart';

Container addDateBar(DateTime _selectedDate) {
  return Container(
    margin: const EdgeInsets.only(top: 20, left: 20),
    child: DatePicker(
      DateTime.now(),
      height: 100,
      width: 80,
      initialSelectedDate: DateTime.now(),
      selectionColor: primaryClr,
      selectedTextColor: Colors.white,
      // dateTextStyle: const TextStyle(
      //   fontSize: 20,
      //   fontWeight: FontWeight.w600,
      //   color: Colors.grey,
      // ),
      dateTextStyle: GoogleFonts.lato(
        textStyle: const TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.w600,
          color: Colors.grey,
        ),
      ),
      dayTextStyle: GoogleFonts.lato(
        textStyle: const TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w600,
          color: Colors.grey,
        ),
      ),
      monthTextStyle: GoogleFonts.lato(
        textStyle: const TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w600,
          color: Colors.grey,
        ),
      ),
      onDateChange: (selectedDate) {
        _selectedDate = selectedDate;
      },
    ),
  );
}
